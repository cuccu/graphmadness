(ns graphmadness.gui
  (:use seesaw.core)
  (:use seesaw.graphics)
  (:use seesaw.border)
  (:use seesaw.keymap)  
  (:use splasher.core)
  (:use graphmadness.conversions)
  (:use graphmadness.functions)  
  (:use graphmadness.grid)
  (:use graphmadness.infodialog)
  (:use clojure.java.io)
  (:gen-class))

(native!)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Atoms
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def unit-x (atom 20))
(def unit-y (atom 20))

(def h-shift (atom 0))
(def v-shift (atom 0))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Painters
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def axis-line-style (style :foreground "#000" :stroke 1 :cap :round))
(def grid-line-style (style :foreground "#999" :stroke 1 :cap :round))
(def inte-line-style (style :foreground "#666" :stroke 1 :cap :round))
(def func-name-style (style :background "#FFF" :foreground "#000"))

(def func-line-styles 
  [(style :foreground "#FF0000" :stroke 1 :cap :round)
   (style :foreground "#088A08" :stroke 1 :cap :round)
   (style :foreground "#0000FF" :stroke 1 :cap :round)
   (style :foreground "orange"  :stroke 1 :cap :round)])

(defn paint-axis
  [c g]
  (let [d 1
        w (width c)
        w2 (+ (/ w 2) @h-shift)
        w2+ (+ w2 5)
        w2- (- w2 5)
        h (height c)
        h2 (+ (/ h 2) @v-shift)
        h2+ (+ h2 5)
        h2- (- h2 5)
        s axis-line-style
        fm (.getFontMetrics g)]
    
    ;; draw x axis
    (draw g (line d h2 w h2) s)
    ;; draw y axis
    (draw g (line w2 d w2 h) s)
    ;; draw x units
    (dorun
      (map
        #(do 
           (draw g (line (- w2 %) h2- (- w2 %) h2+) s)
           (draw g (line (+ w2 %) h2- (+ w2 %) h2+) s)
           (let [s (str (pixel-to-x (- w2 %) w2 @unit-x))
                 sw (/ (.stringWidth fm s) 2)
                 sh (.getHeight fm)]
             (.drawString g s (- (int (- w2 %)) sw) (+ sh h2+)))
           (let [s (str (pixel-to-x (+ w2 %) w2 @unit-x))
                 sw (/ (.stringWidth fm s) 2)
                 sh (.getHeight fm)]
             (.drawString g s (- (int (+ w2 %)) sw) (+ sh h2+))))
        (range @unit-x w @unit-x)))
    ;; draw y units
    (dorun
      (map
        #(do 
           (draw g (line w2- (- h2 %) w2+ (- h2 %)) s)
           (draw g (line w2- (+ h2 %) w2+ (+ h2 %)) s)
           (let [s (str (pixel-to-y (- h2 %) h2 @unit-y))
                 sw (.stringWidth fm s)
                 sh (/ (.getHeight fm) 2)]
             (.drawString g s (- (int w2-) sw) (int (+ (- h2- %) sh))))
           (let [s (str (pixel-to-y (+ h2 %) h2 @unit-y))
                 sw (.stringWidth fm s)
                 sh (/ (.getHeight fm) 2)]
             (.drawString g s (- (int w2-) sw) (int (+ (+ h2- %) sh)))))
        (range @unit-y h @unit-y)))))

(defn paint-grid
  [c g]
  (let [w (width c)
        w2 (+ (/ w 2) @h-shift)
        h (height c)
        h2 (+ (/ h 2) @v-shift)
        s grid-line-style]
    (dorun
      (map
        #(draw g (line (- w2 %) 0 (- w2 %) h) s (line (+ w2 %) 0 (+ w2 %) h) s)
        (range 0 w @unit-x)))
    (dorun
      (map
        #(draw g (line 0 (- h2 %) w (- h2 %)) s (line 0 (+ h2 %) w (+ h2 %)) s)
        (range 0 h @unit-y)))))

(defn paint-function-name
  [g line-style func-idx]
  (let [name (:name (nth @functions func-idx))]
    (push 
      g
      (.setColor g (:foreground line-style))
      (.drawString g name 10 (+ 20 (* 20 func-idx))))))

(defn max-func-name-width
  [g]
  (let [fm (.getFontMetrics g)]
    (apply max (map (fn [f] (.stringWidth fm (:name f))) @functions))))

(defn paint-function-names-box
  [g]
  (if (seq @functions)
    (draw 
      g 
      (rect 5 5 (+ 10 (max-func-name-width g)) (* 20 (count @functions)))
      func-name-style)))

(defn paint-functions
  [c g]
  (let [w (width c)
        h (height c)
        w2 (+ (/ w 2) @h-shift)
        h2 (+ (/ h 2) @v-shift)]      
    
    (paint-function-names-box g)
    
    (loop [func-idx 0]
      (when (< func-idx (count @functions))
        (let [f (eval-parsed-function func-idx w w2 h2 @unit-x)
              from (:from (nth @functions func-idx))
              to (:to (nth @functions func-idx))
              integral (integral? func-idx)
              color-idx (mod func-idx (count func-line-styles))
              line-style (nth func-line-styles color-idx)]
          
          (paint-function-name g line-style func-idx)
          
          (loop [x 0]
            (when (< x (dec w))
              (let [f1 (y-to-pixel (nth f x) h2 @unit-y) 
                    f2 (y-to-pixel (nth f (inc x)) h2 @unit-y)]
                (cond

                  integral
                  (if (in-codomain? f1 f2 h)
                    (do
                      (if (in-range? x w2 from to @unit-x)
                        (draw g (line x h2 x f2) inte-line-style))
                      (draw g (line x f1 (inc x) f2) line-style)))

                  :else
                  (if (and (in-codomain? f1 f2 h) (in-range? x w2 from to @unit-x))
                    (draw g (line x f1 (inc x) f2) line-style))))

              (recur (+ x 1)))))
        (recur (inc func-idx))))))

(defn paint-all
  [c g]
  (if (grid-visible?) (paint-grid c g))  
  (paint-axis c g)
  (paint-functions c g))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Components
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def pnl-graph
  (flow-panel
    :paint paint-all
    :background "#EED6AF"
    :border (line-border :color :black :thickness 1)))

(def txt-func (text))

(def txt-from (text))

(def txt-to (text))

(def lbl-position 
  (label 
    :h-text-position :center
    :v-text-position :center
    :text ""))

(def btn-draw 
  (button 
    :text ::btn-draw))

(def btn-derivative-1
  (button 
    :text ::btn-derivative-1))

(def btn-derivative-2 
  (button 
    :text ::btn-derivative-2))

(def btn-integral 
  (button 
    :text ::btn-integral))

(def slider-x
  (slider
    :orientation :vertical
    :snap-to-ticks? true
    :paint-ticks? true
    :paint-labels? true
    :min 10
    :max 200
    :value 20))

(def slider-y
  (slider
    :orientation :vertical
    :snap-to-ticks? true
    :paint-ticks? true
    :paint-labels? true
    :min 10
    :max 200
    :value 20))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Containers
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def pnl-south
  (border-panel
    :center txt-func    
    :west "f(x)="))

(defn my-vertical-panel
  [items]
  (border-panel :north (vertical-panel :items items)))

(def pnl-buttons
  (border-panel
    :west slider-x
    :east slider-y
    :north (my-vertical-panel
             [(border-panel :center btn-draw)
              (border-panel :center btn-derivative-1)
              (border-panel :center btn-derivative-2)
              (border-panel :center btn-integral)
              (border-panel :center "From:")
              (border-panel :center txt-from)
              (border-panel :center "To:")
              (border-panel :center txt-to)
              (border-panel :center lbl-position)])))
    

(def pnl-main
  (border-panel
    :center pnl-graph
    :south pnl-south  
    :east pnl-buttons
    :border 5
    :vgap 5 
    :hgap 5))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Listeners
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn general-listener-draw
  [e callback degree]
  (if (seq (text txt-func))
    (try
      (do
        (callback (text txt-func) degree (text txt-from) (text txt-to))
        (repaint! pnl-graph))
      (catch Exception e (alert pnl-graph (.getMessage e))))))

(defn general-listener
  [callback]
  (do
    (callback)
    (repaint! pnl-graph)))

(defn listener-draw
  [e]
  (general-listener-draw e add-function! 0))

(defn listener-derivative-1
  [e]
  (general-listener-draw e add-function! 1))

(defn listener-derivative-2
  [e]
  (general-listener-draw e add-function! 2))

(defn listener-integral
  [e]
  (general-listener-draw e add-function! -1))

(defn listener-empty-functions
  [e]
  (general-listener empty-functions!))

(defn listener-slider-x
  [e]
  (general-listener (fn [] (swap! unit-x (fn [n] (value e))))))

(defn listener-slider-y
  [e]
  (general-listener (fn [] (swap! unit-y (fn [n] (value e))))))

(defn listener-grid-change
  [e]
  (general-listener swap-grid!))

(defn listener-mouse-moved
  [e]
  (let [h2 (/ (.getHeight pnl-graph) 2)
        w2 (/ (.getWidth pnl-graph) 2)
        x (double (pixel-to-x (- (.getX e) @h-shift) w2 @unit-x))
        y (double (pixel-to-y (- (.getY e) @v-shift) h2 @unit-y))] 
    (text! lbl-position (format "%.2f:%.2f" x y))))

(defn listener-exit
  [e]
  (System/exit 0))

(defn listener-help
  [e]
  (dialog-info pnl-graph ::dlg-title-help "help.txt" [400 :by 400]))

(defn listener-about
  [e]
  (dialog-info pnl-graph ::dlg-title-about "about.txt" [400 :by 250]))

(defn listener-shift
  [direction]
  (do
    (cond
      (= "right" direction)
      (swap! h-shift (fn [n] (+ n @unit-y)))
      (= "left" direction)
      (swap! h-shift (fn [n] (- n @unit-y)))
      (= "up" direction)
      (swap! v-shift (fn [n] (- n @unit-x)))
      (= "down" direction)
      (swap! v-shift (fn [n] (+ n @unit-x))))
      (repaint! pnl-graph)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main and general stuff
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn center!
  [frame]
  (.setLocationRelativeTo frame nil))

(defn bind-keys!
  [frame]
  (map-key frame "F1" listener-help)
  (map-key frame "control G" listener-grid-change)
  (map-key frame "control E" listener-empty-functions)
  (map-key frame "LEFT" (fn [f] (listener-shift "left")))
  (map-key frame "RIGHT" (fn [f] (listener-shift "right")))
  (map-key frame "UP" (fn [f] (listener-shift "up")))
  (map-key frame "DOWN" (fn [f] (listener-shift "down"))))

(def menu-bar
  (menubar
    :items [(menu
              :text ::menu-file
              :items [(menu-item
                        :text ::menu-file-exit
                        :listen [:action listener-exit])])
            (menu
              :text ::menu-edit
              :items [(menu-item
                        :text ::menu-edit-swap-grid
                        :listen [:action listener-grid-change])
                      (menu-item
                        :text ::menu-edit-clean-graph
                        :listen [:action listener-empty-functions])])
            (menu
              :text ::menu-draw
              :items [(menu-item
                        :text ::menu-draw-function
                        :listen [:action listener-draw])
                      (menu-item
                        :text ::menu-draw-derivative-1
                        :listen [:action listener-derivative-1])
                      (menu-item
                        :text ::menu-draw-derivative-2
                        :listen [:action listener-derivative-2])
                      (menu-item
                        :text ::menu-draw-integral
                        :listen [:action listener-integral])])
            (menu
              :text ::menu-help
              :items [(menu-item
                        :text ::menu-help-help
                        :listen [:action listener-help])
                      (menu-item 
                        :text ::menu-help-about
                        :listen [:action listener-about])])]))

(defn -main
  [& args]
  (splash! (clojure.java.io/resource "splash.png") :duration 2000)
  (invoke-later
    (doto 
      (frame 
        :title ::frame-title
        :content pnl-main
        :on-close :exit
        :menubar menu-bar
        :icon (icon (resource "icon.png"))
        :size [700 :by 450])
      bind-keys!
      center!
      show!)))

(listen btn-draw :action listener-draw)
(listen btn-derivative-1 :action listener-derivative-1)
(listen btn-derivative-2 :action listener-derivative-2)
(listen btn-integral :action listener-integral)
(listen slider-x :change listener-slider-x)
(listen slider-y :change listener-slider-y)
(listen pnl-graph :mouse-moved listener-mouse-moved)
(map-key txt-func "ENTER" listener-draw)
