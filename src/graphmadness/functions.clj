(ns graphmadness.functions
  (:use seesaw.core)
  (:use mathmadness.core)
  (:use graphmadness.conversions)
  (:use graphmadness.names))

(def functions (atom []))

(def txt-error-integral-range (config (label ::error-integral-range) :text))

(def txt-error-bad-range (config (label ::error-bad-range) :text))

(def txt-error-bad-function (config (label ::error-bad-function) :text))

(def txt-error-overlapped-range (config (label ::error-overlapped-range) :text))

(defn integral-value-text
  [func degree from to]
  (cond 
    (= -1 degree) 
    (format ": %.2f" (to-integral func from to))
    :else 
    ""))

(defn range-value
  [txt degree err]
  (try
    (-> txt (to-func) (to-val 1))
    (catch Exception e
      (cond
        (= -1 degree)
        (throw (Exception. txt-error-integral-range))
        (seq txt)
        (throw (Exception. txt-error-bad-range))
        :else
        err))))

(defn to-func-wrapper
  [function]
  (try
    (to-func function)
    (catch Exception e (throw (Exception. txt-error-bad-function)))))

(defn add-function!
  [function degree from to]
  (let [func (to-func-wrapper function)
        a (range-value from degree (Double/NEGATIVE_INFINITY))
        b (range-value to degree (Double/POSITIVE_INFINITY))]
    (if (<= b a)
      (throw (Exception. txt-error-overlapped-range)))
    (swap!
      functions
      #(conj %1 {:func func
                 :degree degree
                 :from a
                 :to b
                 :name (format
                      "%s%s"
                      (f-name function degree from to)
                      (integral-value-text func degree a b))}))))

(defn empty-functions!
  []
  (swap! functions #(empty %1))) 

(defn integral?
  [func-idx]
  (= -1 (:degree (nth @functions func-idx))))

(defn eval-derivate 
  [f delta] 
  (if (> (count f) 1)
    (concat 
      [(/ (- (second f) (first f)) delta)] 
      (eval-derivate (rest f) delta))
    [(Double/NaN)]))

(defn eval-parsed-function
  [func-idx max-pixel half-width half-height step-x]
  (let [x-axis (map (fn [p] (pixel-to-x p half-width step-x)) (range max-pixel))
        delta (- (pixel-to-x 1 half-width step-x) (pixel-to-x 0 half-width step-x))
        f (nth @functions func-idx)
        f0 (to-vals (:func f) x-axis)]
    
    (cond
      (= -1 (:degree f)) f0
      (=  0 (:degree f)) f0
      (=  1 (:degree f)) (eval-derivate f0 delta)
      (=  2 (:degree f)) (eval-derivate (eval-derivate f0 delta) delta))))
