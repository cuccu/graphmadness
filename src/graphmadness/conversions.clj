(ns graphmadness.conversions)

(defn pixel-to-x
  [pixel half-width step-x]   
  (/ (- pixel half-width) step-x))

(defn pixel-to-y
  [pixel half-height step-y]   
  (/ (- half-height pixel) step-y))

(defn y-to-pixel
  [y half-height step-y]   
  (+ (* (- 0 y) step-y) half-height))

(defn in-range?
  [pixel w2 from to step-x]
  (< from (pixel-to-x pixel w2 step-x) to))

(defn in-codomain? 
  [f1 f2 h]
  (and (< 0 f1 h) (< 0 f2 h)))