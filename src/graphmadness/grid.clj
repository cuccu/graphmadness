(ns graphmadness.grid)

(def grid-visible (atom true))

(defn grid-visible?
  []
  (true? @grid-visible))

(defn swap-grid!
  []
  (swap! grid-visible (fn [n] (not n))))