(ns graphmadness.infodialog
  (:use seesaw.core)
  (:use clojure.java.io))

(native!)

(defn ^:private editor-info
  [filename]
  (scrollable
    (doto
      (editor-pane
        :content-type "text/html")
      (.setEditable false)
      (.setOpaque false)
      (.setPage (resource filename)))))

(defn dialog-info
  [parent title filename size]
  (doto 
    (dialog       
      :size size
      :title title
      :content (editor-info filename))
    (.setLocationRelativeTo parent)
    show!))