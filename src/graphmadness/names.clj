(ns graphmadness.names)

(defn range-name
  [from to]
    (cond
      (and (seq from) (seq to)) (format "[%s:%s]" from to)
      (seq from) (format "[%s:∞]" from)
      (seq to) (format "[-∞:%s]" to)
      :else ""))

(defn degree-name
  [degree]
  (cond 
    (= degree -1) "F"
    (= degree  0) "f"
    (= degree  1) "f'"
    (= degree  2) "f''"
    :else ""))

(defn f-name
  [function degree from to]
  (format "%s(x)%s=%s" (degree-name degree) (range-name from to) function))
