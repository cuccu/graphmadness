(ns graphmadness.grid-test
  (:require [clojure.test :refer :all]
            [graphmadness.grid :refer :all]))

(deftest grid-test
  (testing 
    "grid"
    (is (true? (grid-visible?)) "should be visible by default")
    (swap-grid!)
    (is (false? (grid-visible?)) "should swap to invisible")
    (swap-grid!)
    (is (true? (grid-visible?)) "should swap to visible")))