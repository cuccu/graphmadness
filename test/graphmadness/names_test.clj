(ns graphmadness.names-test
  (:require [clojure.test :refer :all]
            [graphmadness.names :refer :all]))

(deftest range-name-test
  (testing 
    "range-name"
    (is (= "" (range-name nil nil)) "should return empty string")
    (is (= "[-∞:123]" (range-name nil "123")) "should return [-∞:123]")
    (is (= "[123:∞]" (range-name "123" nil)) "should return [123:∞]")
    (is (= "[123:456]" (range-name "123" "456")) "should return [123:456]")))

(deftest degree-name-test
  (testing 
    "degree-name"
    (is (= "F" (degree-name -1)) "should return degree -1")
    (is (= "f" (degree-name 0)) "should return degree 0")
    (is (= "f'" (degree-name 1)) "should return degree 1")
    (is (= "f''" (degree-name 2)) "should return degree 2")
    (is (= "" (degree-name 3)) "should return degree 0")
    (is (= "" (degree-name "a")) "should return degree 0")))

(deftest f-name-test
  (testing 
    "f-name"
    (is (= "F(x)[1:3]=fff" (f-name "fff" -1 "1" "3")) "should return degree F(x)=fff")
    (is (= "f(x)=fff" (f-name "fff" 0 nil nil)) "should return degree f(x)=fff")
    (is (= "f'(x)=fff" (f-name "fff" 1 nil nil)) "should return degree f'(x)=fff")
    (is (= "f''(x)=fff" (f-name "fff" 2 nil nil)) "should return degree f''(x)=fff")
    (is (= "f(x)[5:∞]=fff" (f-name "fff" 0 "5" nil)) "should return degree f(x)[5.0-∞]=fff")
    (is (= "f'(x)[-∞:5]=fff" (f-name "fff" 1 nil "5")) "should return degree f'(x)[-∞-5.0]=fff")
    (is (= "f''(x)[-5:5]=fff" (f-name "fff" 2 "-5" "5")) "should return degree f''(x)[-5.0-5.0]=fff")))