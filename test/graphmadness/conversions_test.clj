(ns graphmadness.conversions-test
  (:require [clojure.test :refer :all]
            [graphmadness.conversions :refer :all]))

(deftest pixel-to-x-test
  (testing 
    "pixel-to-x"
    (is (= -4 (pixel-to-x 0 200 50)) "should convert to x")
    (is (= 0 (pixel-to-x 200 200 50)) "should convert to x")
    (is (= 4 (pixel-to-x 400 200 50)) "should convert to x")))

(deftest pixel-to-y-test
  (testing 
    "pixel-to-y"
    (is (= 4 (pixel-to-y 0 200 50)) "should convert to y")
    (is (= 0 (pixel-to-y 200 200 50)) "should convert to y")
    (is (= -4 (pixel-to-y 400 200 50)) "should convert to y")))

(deftest y-to-pixel-test
  (testing 
    "y-to-pixel"
    (is (= 0 (y-to-pixel 4 200 50)) "should convert to pixel")
    (is (= 200 (y-to-pixel 0 200 50)) "should convert to pixel")
    (is (= 400 (y-to-pixel -4 200 50)) "should convert to pixel")))

(deftest in-range?-test
  (testing 
    "in-range?"
    (is (false? (in-range? 0 200 -2 2 50)) "should return false")
    (is (false? (in-range? 99 200 -2 2 50)) "should return false")
    (is (false? (in-range? 100 200 -2 2 50)) "should return false")
    (is (true? (in-range? 101 200 -2 2 50)) "should return true")
    (is (true? (in-range? 299 200 -2 2 50)) "should return true")
    (is (false? (in-range? 300 200 -2 2 50)) "should return true")
    (is (true? (in-range? 0 200 (Double/NEGATIVE_INFINITY) 2  50)) "should return true")
    (is (true? (in-range? 999 200 -2 (Double/POSITIVE_INFINITY) 50)) "should return true")))

(deftest in-codomain?-test
  (testing 
    "in-codomain?"
    (is (true? (in-codomain? 50 60 100)) "should return true")
    (is (true? (in-codomain? 1 60 100)) "should return true")
    (is (true? (in-codomain? 50 1 100)) "should return true")
    (is (true? (in-codomain? 99 60 100)) "should return true")
    (is (true? (in-codomain? 50 99 100)) "should return true")
    (is (true? (in-codomain? 99 99 100)) "should return true")
    
    (is (false? (in-codomain? 0 60 100)) "should return false")
    (is (false? (in-codomain? 50 0 100)) "should return false")
    (is (false? (in-codomain? 0 0 100)) "should return false")
    (is (false? (in-codomain? 101 60 100)) "should return false")
    (is (false? (in-codomain? 50 101 100)) "should return false")
    (is (false? (in-codomain? 101 101 100)) "should return false")))



