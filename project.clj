(defproject graph "0.0.5-SNAPSHOT"
  :description "A simple app for your math graphs"
  :url "https://bitbucket.org/cuccu/graphmadness"
  :license {:name "Public Domain" :url "http://unlicense.org/UNLICENSE"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [seesaw "1.4.5"]
                 [eigenhombre/splasher "0.0.2"]
                 [org.clojars.cuccu/mathmadness "0.0.1"]]
  :javac-options ["-target" "1.6" "-source" "1.6" "-Xlint:-options"]
  :aot [graphmadness.gui]
  :main graphmadness.gui)
